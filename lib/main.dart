import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  } 
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool loading = false;
  List<DateTime> dateList = [];

  /// Date list with a duration of 15 min.
  List<DateTime> dateList15Min = [];

  /// Date list with a duration of 1 hour.
  List<DateTime> dateList1H = [];

  /// Date list with a duration of 2 hour.
  List<DateTime> dateList2H = [];

  /// Date list with a duration of 3 hour.
  List<DateTime> dateList3H = [];

  /// Date list with a duration of 4 hour.
  List<DateTime> dateList4H = [];

  /// Date list with a duration of 6 hour.
  List<DateTime> dateList6H = [];

  /// Date list with a duration of 8 hour.
  List<DateTime> dateList8H = [];

  /// Date list with a duration of 12 hour.
  List<DateTime> dateList12H = [];

  /// Date list with a duration of 1 days.
  List<DateTime> dateList1D = [];

  DateTime startDate = DateTime(2021, 5, 1, 0, 0);
  DateTime endDate = DateTime(2022, 5, 1, 0, 0);

  generateDates() async {
    loading = true;
    setState(() {});

    print("Generating Dates");
    DateTime currentDate = startDate;
    dateList.clear();

    while (currentDate.isBefore(endDate)) {
      dateList.add(currentDate);
      currentDate = currentDate.add(Duration(minutes: 15));
    }

    //15min
    dateList15Min = dateList;

    //1H
    dateList1H = dateList.where((d) => d.minute == 0).toList();

    //2H - 12H
    dateList2H =
        dateList.where((d) => d.hour % 2 == 0 && d.minute == 0).toList();

    dateList3H =
        dateList.where((d) => d.hour % 3 == 0 && d.minute == 0).toList();

    dateList4H =
        dateList.where((d) => d.hour % 4 == 0 && d.minute == 0).toList();

    dateList6H =
        dateList.where((d) => d.hour % 6 == 0 && d.minute == 0).toList();

    dateList8H =
        dateList.where((d) => d.hour % 8 == 0 && d.minute == 0).toList();

    dateList12H =
        dateList.where((d) => d.hour % 12 == 0 && d.minute == 0).toList();

    dateList1D = dateList.where((d) => d.hour == 0 && d.minute == 0).toList();

    print("Generating DateString");

    var dateList15MinString = getDateString(dateList15Min);
    var dateList1HString = getDateString(dateList1H);
    var dateList2HString = getDateString(dateList2H);
    var dateList3HString = getDateString(dateList3H);
    var dateList4HString = getDateString(dateList4H);
    var dateList6HString = getDateString(dateList6H);
    var dateList8HString = getDateString(dateList8H);
    var dateList12HString = getDateString(dateList12H);
    var dateList1DString = getDateString(dateList1D);

    var content = """
      List<DateTime> dateList15Min = [
        $dateList15MinString
      ];

      List<DateTime> dateList1H = [
        $dateList1HString
      ];

      List<DateTime> dateList2H = [
        $dateList2HString
      ];

      List<DateTime> dateList3H = [
        $dateList3HString
      ];

      List<DateTime> dateList4H = [
        $dateList4HString
      ];

      List<DateTime> dateList6H = [
        $dateList6HString
      ];

      List<DateTime> dateList8H = [
        $dateList8HString
      ];

      List<DateTime> dateList12H = [
        $dateList12HString
      ];

       List<DateTime> dateList1D = [
        $dateList1DString
      ];
    """;

    var file = File("lib/date_list.dart");
    file.writeAsStringSync(content);
    loading = false;
    setState(() {});
  }

  String getDateString(List pDateList) {
    var dateListString = "";
    pDateList.forEach((date) {
      var dateString = DateFormat("y,M,d").format(date);
      dateListString += "DateTime($dateString),\n";
    });
    return dateListString;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Date Generator"),
      ),
      body: loading
          ? Center(
              child: Text("Loading..."),
            )
          : Column(
              children: [
                Container(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      Text("Date Count: ${dateList.length}"),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: Scrollbar(
                      isAlwaysShown: true,
                      child: ListView.builder(
                        itemCount: dateList.length,
                        itemBuilder: (context, index) {
                          var date = dateList[index];
                          return Container(
                            child: Text("$index - $date"),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
      floatingActionButton: loading
          ? Container()
          : FloatingActionButton(
              onPressed: () => generateDates(),
              tooltip: 'Increment',
              child: Icon(Icons.add),
            ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
